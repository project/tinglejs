<?php

namespace Drupal\tinglejs\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\tinglejs\Exception\LibraryException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Tingle.js settings for this site.
 */
final class SettingsForm extends ConfigFormBase
{

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'tinglejs.settings';

  /**
   * The library.discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Constructs a form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config.factory service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library.discovery service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LibraryDiscoveryInterface $library_discovery
  ) {
    $this->libraryDiscovery = $library_discovery;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('library.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'tinglejs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [static::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config(static::SETTINGS);
    $form['tinglejs'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tingle.js library configuration'),
      '#tree' => TRUE,
      '#description' => $this->t(
        'Configure the source and version used for the Tingle.js library. The Tingle.js library machine name is <code>"tinglejs/tinglejs"</code>, <a href="@drupalorgLibraryApiManualUrl" target="_blank">see the Drupal.org library API page for information how to load libraries in themes or modules</a>.',
        ['@drupalorgLibraryApiManualUrl' => 'https://www.drupal.org/docs/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-module']
      ),
    ];

    $form['tinglejs']['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Library source'),
      '#options' => [
        // Do not provide this library:
        'disabled' => $this->t('Disabled'),
        'local' => $this->t('Local library (manual download)'),
        // Local library downloaded via composer using bower-asset/tingle:
        // See https://www.drupal.org/docs/8/modules/lazy-load/how-to-use-composer-to-install-lazy-load-module-and-its-dependency
        'local_composer_npm' => $this->t('Local library via Composer (Asset Packagist: NPM)'),
        'local_composer_bower' => $this->t('Local library via Composer (Asset Packagist: Bower)'),
        // Load from CDN: CDNJS:
        'cdnjs' => $this->t('CDNJS'),
        // Load from CDN: jsDelivr:
        'jsdelivr' => $this->t('jsDelivr'),
      ],
      '#default_value' => $config->get('source'),
      '#description' => $this->t("<strong>Source options:</strong><dl>
        <dt>Disabled:</dt><dd>Do not provide /load this library at all</dd>
        <dt>Local library (manual download):</dt><dd>Download the library manually and place it in <code>libraries/tinglejs</code> (resulting in <code>libraries/tinglejs/dist/tingle(.min).js</code>)</dd>
        <dt>Local via Composer (Asset Packagist: NPM):</dt><dd>Download libraries by <code>composer require npm-asset/tingle.js:@version</code> to libraries/tingle.js/dist/tingle(.min).js. See <a href='@composerAssetsInfoUrl'>this drupal.org Documentation</a> for details.</dd>
        <dt>Local via Composer (Asset Packagist: Bower):</dt><dd>Download libraries by <code>composer require bower-asset/tingle:@version</code> to libraries/tingle/dist/tingle(.min).js. See <a href='@composerAssetsInfoUrl'>this drupal.org Documentation</a> for details.</dd>
        <dt>CDNJS:</dt><dd>Loads the library from remote CDN <a href='https://cdnjs.com/'>https://cdnjs.com/</a></dd>
        <dt>jsDelivr:</dt><dd>Loads the library from remote CDM <a href='https://www.jsdelivr.com/'>https://www.jsdelivr.com/</a></dd>
        </dl>
        ", [
        '@composerAssetsInfoUrl' => 'https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries',
        '@version' => $config->get('version'),
      ]),
    ];

    $form['tinglejs']['development'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development version'),
      '#description' => $this->t('Load unminified source files.'),
      '#default_value' => $config->get('development'),
      '#states' => [
        'invisible' => [
          'select[name="tinglejs[source]"]' => [
            'value' => 'disabled',
          ],
        ],
      ],
    ];

    $form['tinglejs']['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#size' => 9,
      '#required' => TRUE,
      '#default_value' => $config->get('version'),
      '#description' => $this->t('For latest version see @githubProjectUrl', ['@githubProjectUrl' => 'https://github.com/robinparisi/tingle/']),
      '#states' => [
        'invisible' => [
          'select[name="tinglejs[source]"]' => [
            ['value' => 'disabled'],
          ],
        ],
      ],
    ];

    $registered_libraries = $this->libraryDiscovery->getLibraryByName('tinglejs', TINGLEJS_LIBRARY_NAME);
    if (!empty($registered_libraries)) {
      $paths = [];
      if (!empty($registered_libraries['js'][0]['data'])) {
        $paths['js'] = $registered_libraries['js'][0]['data'];
      }
      if (!empty($registered_libraries['css'][0]['data'])) {
        $paths['css'] = $registered_libraries['css'][0]['data'];
      }

      $urls = [];
      foreach ($paths as $type => $path) {
        $options = [
          'absolute' => TRUE,
          'attributes' => ['target' => '_blank'],
        ];
        if (strpos($path, 'libraries') === 0) {
          $url = Url::fromUserInput('/' . $path, $options);
        } else {
          $url = Url::fromUri($path, $options);
        }

        $urls[$path] = (new Link($url->toString(), $url))->toString();
      }

      $form['urls'] = [
        '#type' => 'markup',
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $this->t('Library source URL Preview'),
        '#description' => $this->t('Preview the URLs loaded by the library based on these settings.'),
        '#items' => $urls,
        '#attributes' => ['class' => 'urls'],
        '#wrapper_attributes' => ['class' => 'container'],
        '#states' => [
          'invisible' => [
            'select[name="source"]' => ['value' => 'disabled'],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $version = $form_state->getValue('version');
    if (!preg_match('/^\d+\.\d+\.\d+$/', $version)) {
      $form_state->setErrorByName('version' . '][version', $this->t('Version format is not correct.'));
    }
    $form_state->setValue('version', $version);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('source', $form_state->getValue('source'))
      ->set('development', $form_state->getValue('development'))
      ->set('version', $form_state->getValue('version'))
      ->save();

    $this->libraryDiscovery->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }
}
