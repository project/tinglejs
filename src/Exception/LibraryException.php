<?php

declare(strict_types = 1);

namespace Drupal\tinglejs\Exception;

/**
 * Class LibraryException is the parent of all exceptions in the Tingle
 * module.
 */
class LibraryException extends \Exception {

}
